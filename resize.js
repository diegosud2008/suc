const path = require('path');
const fs = require('fs');
const sharp = require('sharp');
const axios = require('axios');
const https = require('https');
const { error } = require('console');


module.exports = async function saveImg(fileparth,mkp) {
    const [filename, extension] = path.basename(fileparth).split(".");
    let buffer;
    const imageResponse = await axios({url: fileparth, responseType: 'arraybuffer', httpsAgent: new https.Agent({ rejectUnauthorized: false })}).catch(error => console.log(error))
    if (imageResponse) {
        buffer = Buffer.from(imageResponse.data, 'binary')
        const destino = `../suc_prod/app/webroot/img/imagens_produtos_mkp`;
    
    if (!fs.existsSync(destino)) {
        fs.mkdirSync(destino);
    }
    
    const sizes = [100];
    await sharp(buffer).clone().resize({width: 1000, height: 1000}).toFile(`${destino}/${mkp}-${filename}.${extension}`)
    .then( info => {
        console.log(info);
    })
    .catch( erro => {
        console.log(erro)
    })
        
    }
    return `imagens_produtos_mkp/${filename}-100.${extension}`;
}



// const destino = `./redimensionado`;

// if (!fs.existsSync(destino)) {
//     fs.mkdirSync(destino);
// }

// const sizes = [100];
// console.log(extension)
// sharp(buffer).clone().resize({width: 1000}).toFile(`${destino}/${filename}-100.${extension}`)
// .then( info => {
//     console.log(info);
// })
// .catch( erro => {
//     console.log(erro)
// })

