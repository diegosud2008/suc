const { default: axios } = require('axios');
const resize = require('./resize');
const connect = require('./db/db');

const BASE_URL = 'http://18.211.180.34/infoar-api/api/integracoes';
const TOKKEN_PORTAL = '3VCmfnjHrS9k';



async function migraProcts(page = '0', recursiva, seller) {
    
    const paramrs = process.argv;
    const cnpj = paramrs[2];
    const start = `${new Date().getFullYear()}-${new Date().getMonth()}-${new Date().getDate()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
    
    const sellers = await getSellers(cnpj);    
    
    
    
    for await (let cnpj of sellers) {
        let response
        page = 0
        do {
            console.log(cnpj,page)
            response = await axios(`${BASE_URL}/produto?token=${TOKKEN_PORTAL}&referencia=${cnpj.cnpj}&suc_read=0&status=1&ativo=1&idWebContinental=1&tamanho=50&pagina=0&alteracaode=2021-01-01T00:09:00Z`).catch(error => console.error(error));
            let produtos = [];
            let produtosPendentes = [];
            let product_erp_all = [];
        
            for await ( let itens of response.data ) {
                let products = organize(itens);
                let valid = validate(products,cnpj.codigo);
                let product = {};
                let img_salve = [];
                
                let status_mid = await findStatusOld(products.sku);
                if (valid == 200) {
                    let main = 0;
                    let ordem = 1;
                    for await ( let imgs of products.imagem) {
                        img_salve.push({
                            url: await resize(imgs,products.sku),
                            sku: products.sku,
                            principal: main,
                            origem: 'PORTAL',
                            ordem: ordem,
                            ativo: 1
                        })
                        
                        main = 1;
                        ordem++;
                    }
                    if (img_salve.length == 0) {
                        valid = 129;
                    }
                    insertImgs(img_salve);
                    product = products;
                    let descricao = format_description(product.descricao);
                    product['descricao']         = `<h2>${product.titulo}</h2><br>${descricao}`;
                    product['cnpj']              = cnpj.cnpj;
                    product['fornecedor']        = itens.empresa.nomeFantasia.replace(/'/g, "\\'");
                    product['marca']             = products['marca'].replace(/'/g, "\\'");
                    product['titulo']            = products['titulo'].replace(/'/g, "\\'");
                    product['descricao_longa']   = products['descricao'].replace(/'/g, "\\'");
                    product['descricao_curta']   = products['descricao'].replace(/'/g, "\\'");
                    product['atualizacao_flexy'] = products['atualizacao'];
                    product['status_mid'] = status_mid[0].status_mid || 2;

                    if (status_mid[0].status_mid == 4 || status_mid[0].status_mid == 7) {
                        produtos.push(product);
                    } else {
                        produtosPendentes.push(product);
                    }

                    product_erp = [];

                    product_erp['sku']         = product['sku'];
                    product_erp['nome']        = product['titulo'];
                    product_erp['grupo']       = 'MKP';
                    product_erp['um']          = 'PC';
                    product_erp['tipo']        = 'MK';
                    product_erp['cpfab']       = itens.codigoReferencia;
                    product_erp['codbarra']    = product['codigo_ean'];
                    product_erp['ncm']         = product['codigo_ncm'];
                    product_erp['web']         = 'S';
                    product_erp['vendidopor']  = itens.empresa.vendidoPor.replace(/'/g, "\\'");
                    product_erp['desconto']    = 0;

                    product_erp_all.push(product_erp);

                    axios(`${BASE_URL}/produto/${itens.codigoReferencia}?token=${TOKKEN_PORTAL}&origin=conector&referencia=93067221000168&suc_read=1&status=1&ativo=1&idWebContinental=1&tamanho=50&pagina=0&alteracaode=2021-01-01T00:09:00Z`,{ method: 'PUT',data: {},headers: { 'Content-Type': 'application/json'}}).catch(error => console.error(error));
                } else {
                    let status_mid = await findStatusOld(products.sku);
                    let rejection = [products.sku];
                    let motive     = await findMotivo(valid);
                    if (status_mid[0].status_mid != 4 || status_mid[0].status_mid != 7) {
                        if (!status_mid[0].status_mid) {
                            motive = "Produto Rejeitado Portal- "+motive;
                            saveActionLog(rejection, motive, 3);
                        }
                        saveStatusProdutoMarketplace(products.sku,valid, cnpj.cnpj, 0, products.referencia)
                        updateStatus(products.sku, 5);
                    } else {
                        motive = "Produto Rejeitado Portal- "+motive;
                        saveActionLog(rejection, motive, 12);
                    }
                }
            }
            
            if (produtos.length > 0) {
                insertProduct(produtos)
                saveProdutoMarketplacePendente(produtosPendentes);
                saveProdutoErp(product_erp_all)
            }
            page++;
        } while (response && response.data.length === 100)
        console.log(cnpj)
    
        // if (response.data.length === 50) {
        //     migraProcts(`${parseInt(page)+1}`)
        // }
        // migraProcts(`${parseInt(page)+1}`,true,cnpj.cnpj)
    }
    console.log(seller)
    
    
    return true;
    
    console.log(new Date())
    return true;
    // const conn = await connect();
    // const [rows] = await conn.query('SELECT * FROM nt_leads;');
    
    // rows.map( linha =>{
    //     console.log(linha.ID)
    // })
    // return rows;
}

async function updateStatus(sku, status){
    const conn = await connect();
    /*
    * 5 - Rejeitado, 2 - Pendente Curadoria
    */
    let update = `UPDATE produtos_flexy set status_mid = '${status}', editar = NOW(), usuario_editar = 0, modificado_mid = NOW() WHERE sku = '${sku}'`;
    conn.query(update);

}

async function saveStatusProdutoMarketplace(sku, status, cnpj, enviado, referencia){
    const conn = await connect();
    let sql = `INSERT INTO status_flexy (sku,status,cnpj,modificado,enviado,referencia) VALUES ('${sku}', ${status}, '${cnpj}', NOW(), ${enviado}, '${referencia}') ON DUPLICATE KEY UPDATE sku = VALUES(sku), status = VALUES(status), modificado = NOW(), enviado = VALUES(enviado), referencia = VALUES(referencia), cnpj = VALUES(cnpj) `;
    conn.query(sql);
}

async function saveActionLog(skus, acao, id) {
    const conn = await connect();
    if (skus) { return false; }
    let sql = "INSERT INTO log_usuarios_suc (id_usuario,nome_usuario,sku_produto, acao,id_acao,criado) VALUES ";
    for await (let sku of skus) {
        sql += `(0,'Sistema', '${sku}', '${acao}',${id}, NOW()),`;
    }
    sql = rtrim(sql, ',');
    conn.query(sql);
}

async function findMotivo(status){
    const conn = await connect();
    let sql 	 = "select motivo from motivo_flexy where id ="+status;
    retorno = await conn.query(sql);
    
    return retorno[0]['motivo'];
}

async function saveProdutoErp(produtos) {
        let update = ''; 

		let sql = `INSERT INTO produtos_erp (
			sku,nome,grupo,um,tipo,cpfab,codbarra,ncm,web,vendidopor,desconto,atualizado
										) VALUES `;
		let virgula = '';
		for await (let dados of produtos) {
			update += virgula+"'"+dados['sku']+"'";
			sql += virgula+`
							('${dados['sku']}',      
							'${dados['nome']}',      
							'${dados['grupo']}',
							'${dados['um']}',
							'${dados['tipo']}',
							'${dados['cpfab']}',
							'${dados['codbarra']}',
							'${dados['ncm']}',
							'${dados['web']}',
							'${dados['vendidopor']}',
							'${dados['desconto']}',
							NOW())`;


			virgula = ',';
		}
		sql +=	` ON DUPLICATE KEY UPDATE
							sku  = VALUES(sku),
							nome = VALUES(nome),
							grupo = VALUES(grupo),
							um = VALUES(um),
							tipo = VALUES(tipo),
							cpfab = VALUES(cpfab),
							codbarra = VALUES(codbarra),
							ncm = VALUES(ncm),
							web = VALUES(web),
							vendidopor = VALUES(vendidopor),
							desconto = VALUES(desconto),
							atualizado = NOW()
							`;
        console.log(sql)
        const conn = await connect();
        await conn.query(sql);
}

function validate(produto, seller = '001912') {
    let valid = 200;
    valid = !produto.sku.includes(`MKP${seller}`) ? 130 : valid;
    valid = !produto.sku.includes('MKP') ? 126: valid

    valid = !produto.referencia ? 130 : valid;
    valid = produto.titulo.length < 20 ? 104: valid
    valid = produto.titulo.length > 200 ? 105: valid
    valid = produto.titulo.split(' ').length < 3 ? 234: valid

    valid = produto.descricao.length < 150 ?  107: valid

    valid = produto.imagem.length == 0 ?  110: valid

    return valid;
}

function organize(response) {
        let product = {};
        product                 = {...product,sku: response.codigoIntegracao || ""};
		product                 = {...product,titulo: response.nome.toLowerCase().replace(/\b[a-z]/g, function(letra) {
            return letra.toUpperCase();
        }) || ""};
		product                 = {...product,descricao: response.descricaoComercial || ""};
		product                 = {...product,referencia: response.codigoReferencia  || ""};
		product              = {...product,codigo_ean:response.ean || ""};
		product              = {...product,codigo_ncm: response.ncm || ""};
		product              = {...product,altura: response.alturaEmbalado || 0};
		product              = {...product,largura: response.larguraEmbalado || 0};
		product              = {...product,profundidade: response.profundidadeEmbalado || 0};
		product              = {...product,peso: response.pesoEmbalado   || 0};
		product              = {...product,garantia: response.garantia || ""};
		product              = {...product,marca: response.marca    || ""};
		product              = {...product,atualizacao: response.dataAlteracao  || ""};

		product              = {...product,ativo: response.indAtivo     || false};
		product              = {...product,bloqueado: response.indBloqueado || false};
		product              = {...product,aprovado: response.indAprovado  || false};
		product              = {...product,temImagem: response.indImagem    || false};
		product              = {...product,pendente: response.indPendente  || false};
		product              = {...product,reprovado: response.indReprovado || false};
		product              = {...product,status: 'pendente'};

		product.descricao  += "<br>"+ response.informacoesTecnicas  || "";

		id = response.id || 0;

        let img = [];

        response.midias.map( imgs => {
            img.push(imgs.url)
        })

        product = {...product,imagem: img};

        return product;
}

async function insertImgs(dados) {
    const conn = await connect();
    let virgula = '';
    let sql = "INSERT INTO imagens_flexy (sku,url,origem,principal,ordem,ativo,modificado,criado) VALUES";

    for await ( let products of dados ) {
        sql += virgula+"('"+products.sku+"','"+products.url+"','PORTAL',"+products.principal+","+products.ordem+","+1+", NOW(), NOW())";
        virgula = ',';
    }
    sql += " ON DUPLICATE KEY UPDATE sku = VALUES(sku), url = VALUES(url), principal = VALUES(principal), modificado = NOW()";
    await conn.query(sql);
    return true;
}
async function insertProduct(produtos) {
    const conn = await connect();
    let update = '';
    let virgula = '';
    let sql = `INSERT INTO produtos_flexy (
        sku,cnpj,titulo_flexy,fornecedor_flexy,referencia_flexy,cod_ean_flexy,cod_ncm_flexy,peso_flexy,altura_flexy,largura_flexy,profundidade_flexy,desc_longa_flexy,desc_curta_flexy,status_flexy,status_mid,atualizacao_flexy,criado,integracao_flexy, garantia, marca
                                    ) VALUES `;
    for await ( let dados of produtos ) {
        update += virgula+"'"+dados['sku']+"'";
        dados['garantia'] = dados['garantia'] || "";
        codigo_ncm = dados['codigo_ncm'] == '00000000' ? '' : dados['codigo_ncm'];
        sql += virgula+`
                        ('${dados["sku"]}',
                        '${dados["cnpj"]}',
                        '${dados["titulo"]}',
                        '${dados['fornecedor']}',
                        '${dados['referencia']}',
                        '${dados['codigo_ean']}',
                        '${codigo_ncm}',
                        ${dados['peso']},
                        ${dados['altura']},
                        ${dados['largura']},
                        ${dados['profundidade']},
                        '${dados['descricao_longa']}',
                        '${dados['descricao_curta']}',
                        '${dados['status']}',
                        '${dados['status_mid']}',
                        '${dados['atualizacao_flexy']}',
                            NOW(),
                            NOW(),
                        '${dados['garantia']}',
                        '${dados['marca']}'
                        )`;


        virgula = ',';
    }
    sql +=	` ON DUPLICATE KEY UPDATE
							sku  = VALUES(sku),
							cnpj = VALUES(cnpj),
							titulo_flexy = VALUES(titulo_flexy),
							fornecedor_flexy = VALUES(fornecedor_flexy),
							referencia_flexy = VALUES(referencia_flexy),
							cod_ean_flexy = VALUES(cod_ean_flexy),
							cod_ncm_flexy = VALUES(cod_ncm_flexy),
							peso_flexy = VALUES(peso_flexy),
							altura_flexy = VALUES(altura_flexy),
							largura_flexy = VALUES(largura_flexy),
							profundidade_flexy = VALUES(profundidade_flexy),
							desc_longa_flexy = VALUES(desc_longa_flexy),
							desc_curta_flexy = VALUES(desc_curta_flexy),
							status_flexy = VALUES(status_flexy),
							atualizacao_flexy = VALUES(atualizacao_flexy),
							integracao_flexy = NOW(),
							garantia = VALUES(garantia),
                            marca = VALUES(marca)
							`;
    await conn.query(sql);    
}

async function getSellers(seller = null) {
    const conn = await connect();
    let where = ''
    if (seller) {
        where = ` AND codigo = ${seller}`
    }

    let sql = `SELECT cnpj,codigo FROM clientes_marketplace WHERE codigo IS NOT NULL ${where}`;

    
    const [rows] = await conn.query(sql);

    return rows;
}
async function format_description(descriptio) {
    let replace = ['<h1>','</h1>','<h2>','</h2>','<h3>','</h3>','<h4>','</h4>','<h5>','</h5>','<u>','</u>','<ul>','</ul>','<li>','</li>'];

    for await (let repla of replace ) {
        var regexp = new RegExp(repla, 'g');
        descriptio = descriptio.replace(regexp,'');
    }

    return replace;
}

async function saveProdutoMarketplacePendente(produtos) {
        const conn = await connect();
        let update = ''; 

		let sql = `INSERT INTO produtos_flexy (
			sku,cnpj,titulo,referencia,cod_ean,cod_ncm,peso,altura,largura,profundidade,desc_longa,desc_curta,status_mid,atualizacao_flexy,criado,integracao_flexy,
			titulo_flexy,fornecedor_flexy,referencia_flexy,cod_ean_flexy,cod_ncm_flexy,peso_flexy,altura_flexy,largura_flexy,profundidade_flexy,desc_longa_flexy,desc_curta_flexy, marca, garantia
										) VALUES `;
		let virgula = '';
		let skus = [];
		for await (let dados of produtos) {
			skus.push(dados['sku']);
			dados['garantia'] = dados['garantia'] || "";
			sql += virgula+`
							('${dados['sku']}',
							'${dados['cnpj']}',
							'${dados['titulo']}',
							'${dados['referencia']}',
							'${dados['codigo_ean']}',
							'${dados['codigo_ncm']}',
							${dados['peso']},
							${dados['altura']},
							${dados['largura']},
							${dados['profundidade']},
							'${dados['descricao_longa']}',
							'${dados['descricao_curta']}',
							'${dados['status_mid']}',
							'${dados['atualizacao_flexy']}',
								NOW(),
								NOW(),
							'${dados['titulo']}',
							'${dados['fornecedor']}',
							'${dados['referencia']}',
							'${dados['codigo_ean']}',
							'${dados['codigo_ncm']}',
							${dados['peso']},
							${dados['altura']},
							${dados['largura']},
							${dados['profundidade']},
							'${dados['descricao_longa']}',
							'${dados['descricao_curta']}',
							${ (dados['marca'] ? 'NULL' : dados.marca)},
							'${dados['garantia']}'
							)`;
			virgula = ',';
		}

		/*
			titulo_flexy,fornecedor_flexy,referencia_flexy,cod_ean_flexy,cod_ncm_flexy,peso_flexy,altura_flexy,largura_flexy,profundidade_flexy,desc_longa_flexy,desc_curta_flexy
		*/
		sql +=	` ON DUPLICATE KEY UPDATE
							sku  = VALUES(sku),
							cnpj = VALUES(cnpj),
							titulo = VALUES(titulo),
							referencia = VALUES(referencia),
							cod_ean = VALUES(cod_ean),
							cod_ncm = VALUES(cod_ncm),
							peso = VALUES(peso),
							altura = VALUES(altura),
							largura = VALUES(largura),
							profundidade = VALUES(profundidade),
							desc_longa = VALUES(desc_longa),
							desc_curta = VALUES(desc_curta),
							status_mid = VALUES(status_mid),
							atualizacao_flexy = VALUES(atualizacao_flexy),
							integracao_flexy = NOW(),
							titulo_flexy = VALUES(titulo_flexy),
							fornecedor_flexy = VALUES(fornecedor_flexy),
							referencia_flexy = VALUES(referencia_flexy),
							cod_ean_flexy = VALUES(cod_ean_flexy),
							cod_ncm_flexy = VALUES(cod_ncm_flexy),
							peso_flexy = VALUES(peso_flexy),
							altura_flexy = VALUES(altura_flexy),
							largura_flexy = VALUES(largura_flexy),
							profundidade_flexy = VALUES(profundidade_flexy),
							desc_longa_flexy = VALUES(desc_longa_flexy),
							desc_curta_flexy = VALUES(desc_curta_flexy),
							marca = VALUES(marca),
							garantia = VALUES(garantia)
							`;
        console.log(sql)
        if ( produtos.length > 0 ) {
            await conn.query(sql);
            let new_ids = `${skus.join()}`;
			// let new_sql = `UPDATE produtos_flexy SET status_mid = 4 WHERE status_mid = 2 AND sku IN ( ${new_ids} )`;
            // await conn.query(new_sql);
        }
}

async function findStatusOld(sku) {
    const conn = await connect();
    let sql = `select status_mid from produtos_flexy where sku = '${sku}'`;
    const [rows] = await conn.query(sql);

    return rows;
}
migraProcts()