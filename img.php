<?php
 class exportImg {
    public function check_image($path){
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/app/webroot/'.$path)){
			return true;
		}else{
			return false;
		}
	}
    // ## METODOS RESPONSAVEIS PELAS IMAGENS DOS PRODUTOS ##
	// public function resizeProductImage($url,$path, $debug = false){

	// 	list($width,$height,$type,$t2) = @getimagesize($url);

	// 	if($debug == true){
	// 		// debug(getimagesize($url));
	// 	}

	// 	if($height >= 475){
	// 		$background = imagecreatetruecolor( 1000, 1000 );

	// 		// sets background to white
	// 		$white = imagecolorallocate( $background, 255, 255, 255 );
	// 		imagefill($background, 0, 0, $white);

	// 			// list( $img, $width, $height ) = $this->resizeImage( $url, 1000, 1000 );

	// 		$y = 0;
	// 		$x = 0;

	// 		if($width > $height){
	// 			$y = round( ( 1000 - $height ) / 2 );
	// 		}elseif($height > $width){
	// 			$x = round( ( 1000 - $width ) / 2 );
	// 		}

	// 		imagecopy( $background, $img, $x, $y, 0, 0, $width, $height );
	// 		imagejpeg( $background, $path, 80 );

	// 		// chmod($path, 0777); // Feita alteração no server para esta operação de permissão (conforme indicação do Jorge) - 26.04.19

	// 		return TRUE;
	// 	}else{
	// 		return FALSE;
	// 	}

	// }
    public function resizeProductImageNew($url,$parth, $debug = false) 
    {
        $filename = $url;
        
        //$filename = 'http://exemplo.com/original.jpg';

        // Largura e altura máximos (máximo, pois como é proporcional, o resultado varia)
        // No caso da pergunta, basta usar $_GET['width'] e $_GET['height'], ou só
        // $_GET['width'] e adaptar a fórmula de proporção abaixo.
        $width = 1000;
        $height = 100;

        // Obtendo o tamanho original
        list($width_orig, $height_orig) = getimagesize($filename);

        echo "dados: ". $filename . "<br>" .$width_orig . '<br>' .$height_orig;
        die;
        // Calculando a proporção
        $ratio_orig = $width_orig/$height_orig;

        if ($width/$height > $ratio_orig) {
            $width = $height*$ratio_orig;
        } else {
            $height = $width/$ratio_orig;
        }

        // O resize propriamente dito. Na verdade, estamos gerando uma nova imagem.
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Gerando a imagem de saída para ver no browser, qualidade 75%:
        header('Content-Type: image/jpeg');
        imagejpeg($image_p, null, 75);

        // Ou, se preferir, Salvando a imagem em arquivo:
        imagejpeg($image_p, 'nova.jpg', 75);
        return false;
    }
}

$img = new exportImg();

$img->resizeProductImageNew('https://portal.hubba.pro/infoar-api/api/produtos/1526808/midias/download/4c3e541c-4108-4c83-80b3-995bbf1c6876.jpg','');