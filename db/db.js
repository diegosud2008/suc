module.exports = async function connect(){
    if(global.connection && global.connection.state !== 'disconnected')
        return global.connection;
 
    const mysql = require("mysql2/promise");
    let password = 'D13g0S0uz@';
    const connection = await mysql.createConnection({host: 'testing.hubba.pro',user: 'diego.souza', password: password,database: 'suc'}).catch( error => console.log(error));
    console.log("Conectou no MySQL!");
    global.connection = connection;
    return connection;
}